package com.azserve.academy.java.jdbc.esercizio;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.stream.Collectors;

import com.azserve.academy.java.jdbc.model.Item;
import com.azserve.academy.java.jdbc.model.MovedItem;

public class Movimento {

	private final Connection connection;

	public Movimento(final Connection connection) {
		this.connection = connection;
	}

	public void add(final int idArticolo, final int quantita) throws SQLException, QuantityException {

		if (quantita < 0 && quantita < -this.giacenza(idArticolo)) {
			throw new QuantityException();
		}

		try (PreparedStatement statement = this.connection.prepareStatement("insert into movement (item_id, amount, date) values (?,?, CURRENT_TIMESTAMP)")) {
			statement.setInt(1, idArticolo);
			statement.setInt(2, quantita);
			statement.executeUpdate();
		}
	}

	private List<MovedItem> findAllMovements() throws SQLException {

		try (PreparedStatement statement = this.connection.prepareStatement("select i.id, i.code, i.description, m.amount, m.date from item i left join movement m on i.id = m.item_id");
				ResultSet rs = statement.executeQuery()) {

			final List<MovedItem> movedItems = new ArrayList<>();

			while (rs.next()) {
				final MovedItem movIt = new MovedItem(rs.getInt("id"), rs.getString("code"), rs.getString("description"), rs.getInt("amount"), rs.getDate("date"));
				movedItems.add(movIt);
			}
			return movedItems;
		}
	}

	public int giacenza(final int itemId) throws SQLException {

		final List<MovedItem> movedItems = this.findAllMovements();

		final int sum = movedItems.stream()
				.filter(x -> x.getId() == itemId)
				.mapToInt(x -> x.getAmount())
				.sum();

		return sum;
	}

	public int giacenzaAtDate(final int itemId, final java.sql.Date sqlDateDayAfter) throws SQLException {

		final List<MovedItem> movedItems = this.findAllMovements();

		final int sum = movedItems.stream()
				.filter(x -> {
					if (x.getId() != itemId) {
						return false;
					}
					final java.sql.Date a = x.getDate();
					if (a != null) { // per qualche motivo mi dice che la data potrebbe essere null???
						return a.before(sqlDateDayAfter);
					}
					return false;
				})
				.mapToInt(MovedItem::getAmount)
				.sum();

		return sum;
	}

	public List<Item> unmovedItems() throws SQLException {

		final List<MovedItem> movedItems = this.findAllMovements();
		List<Item> items = new ArrayList<>();

		items = movedItems.stream()
				.filter(x -> x.getDate() == null)
				.map(x -> new Item(x.getId(), x.getCode(), x.getDescription()))
				.collect(ArrayList::new, ArrayList::add, ArrayList::addAll);
		// .toList() non é meglio?

		return items;
	}

	public List<Item> fewItems() throws SQLException {

		final List<MovedItem> movedItems = this.findAllMovements();
		List<Item> items = new ArrayList<>();

		items = movedItems.stream()
				.collect(Collectors.groupingBy(MovedItem::getItem, Collectors.summingInt(MovedItem::getAmount)))
				.entrySet().stream()
				.filter(x -> x.getValue() < 10) // "The expression of type Integer is unboxed into int", come faccio a evitarlo??
				.collect(ArrayList::new, (c, el) -> c.add(el.getKey()), ArrayList::addAll);
		return items;
	}

	public List<Item> articlesMovedLatestMonth() throws SQLException {

		final List<MovedItem> movedItems = this.findAllMovements();
		List<Item> items = new ArrayList<>();

		final Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.MONTH, -1);
		calendar.add(Calendar.DAY_OF_MONTH, -1);
		final java.sql.Date monthBefore = new Date(calendar.getTimeInMillis());

		items = movedItems.stream()
				.filter(x -> { // anche qua mi dice che getDate potrebbe dare null
					java.sql.Date date = x.getDate();
					if (date != null) {
						return date.after(monthBefore);
					}
					return false;
				})
				.map(MovedItem::getItem) // volevo fare la groupiungBy per getItem e per ciascun gruppo associare la data piu alta
				.distinct()
				.collect(ArrayList::new, ArrayList::add, ArrayList::addAll);

		return items;
	}

	public List<MovedItem> latestMonthMovements() throws SQLException {

		List<MovedItem> movedItems = this.findAllMovements();

		final Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.MONTH, -1);
		calendar.add(Calendar.DAY_OF_MONTH, -1);
		final java.sql.Date monthBefore = new Date(calendar.getTimeInMillis());

		movedItems = movedItems.stream()
				.filter(x -> { // anche qua mi dice che getDate potrebbe dare null
					java.sql.Date date = x.getDate();
					if (date != null) {
						return date.after(monthBefore);
					}
					return false;
				})
				.toList();

		return movedItems;
	}
}
