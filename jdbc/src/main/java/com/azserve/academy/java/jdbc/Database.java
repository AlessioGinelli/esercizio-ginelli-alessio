package com.azserve.academy.java.jdbc;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Random;
import java.util.Scanner;
import java.util.regex.Pattern;

import com.azserve.academy.java.jdbc.ddl.Tables;
import com.azserve.academy.java.jdbc.esercizio.Movimento;
import com.azserve.academy.java.jdbc.esercizio.QuantityException;
import com.azserve.academy.java.jdbc.model.Item;
import com.azserve.academy.java.jdbc.model.ItemService;
import com.azserve.academy.java.jdbc.model.MovedItem;

public class Database {

	/*
	 * postgres=# create role test with login password 'test';
	 * CREATE ROLE
	 * postgres=# create database test with owner test;
	 * CREATE DATABASE
	 *
	 */

	public static void main(final String[] args) throws SQLException {
		final String host = "localhost";
		final String database = "test";
		final String username = "test";
		final String password = "test";

		try (Connection connection = DriverManager.getConnection("jdbc:postgresql://" + host + "/" + database, username, password);
				Scanner scanner = new Scanner(System.in)) {
			int command;
			while ((command = readCommand(scanner)) != 0) {
				switch (command) {
					case 1:
						createTable(connection);
						break;
					case 2:
						insertRandomItem(connection, 10);
						break;
					case 3:
						findItem(connection, scanner);
						break;
					case 4:
						searchAllItems(connection, scanner);
						break;
					case 5:
						searchItems(connection, scanner);
						break;
					case 6:
						addMovement(connection, scanner);
						break;
					case 7:
						giacenza(connection, scanner);
						break;
					case 8:
						giacenzaAtSomeTime(connection, scanner);
						break;
					case 9:
						unmovedItems(connection, scanner);
						break;
					case 10:
						fewItems(connection, scanner);
						break;
					case 11:
						articlesMovedLatestMonth(connection, scanner);
						break;
					case 12:
						latestMonthMovements(connection, scanner);
						break;
					default:
						System.out.println("Invalid command: " + command);
				}
			}

			System.out.println("Bye bye");
		}

	}

	private static int readCommand(final Scanner scanner) {

		System.out.println("""
				1 - create table
				2 - fill with random items
				3 - find item with id
				4 - search all items
				5 - search items by description
				6 - insert a movement
				7 - find how many items have a certain id
				8 - find how many items have a certain id at a certain date
				9 - find the items that have never been moved
				10 - find all items where giacenza is less than 10
				11 - find all items with a movement whithin the last month
				12 - print all items with a movement whithin the last month with their date and amount
				0 - quit
				""");

		return scanner.nextInt();
	}

	private static void createTable(final Connection connection) throws SQLException {
		new Tables(connection).create();
	}

	private static void insertRandomItem(final Connection connection, final int count) throws SQLException {
		final Random r = new Random();
		final ItemService itemService = new ItemService(connection);
		for (int i = 0; i < count; i++) {
			final String code = Utils.padLeft(Math.abs(r.nextInt()) + "", 10, '0');
			itemService.addItem(code, "item with code " + code);
		}
	}

	private static void findItem(final Connection connection, final Scanner scanner) throws SQLException {
		System.out.println("id:");
		final int id = scanner.nextInt();
		final ItemService itemService = new ItemService(connection);
		final Item item = itemService.getItem(id);
		if (item == null) {
			System.out.println("Item with id " + id + " not found");
		} else {
			System.out.println(item);
		}
	}

	private static void searchAllItems(final Connection connection, final Scanner scanner) throws SQLException {
		final ItemService itemService = new ItemService(connection);
		final List<Item> item = itemService.searchItem("%");
		item.stream().forEach(System.out::println);

		// mio
		System.out.println("type in any key to continue");
		scanner.next();
	}

	private static void searchItems(final Connection connection, final Scanner scanner) throws SQLException {
		System.out.println("description:");
		final String description = scanner.next();
		final ItemService itemService = new ItemService(connection);
		final List<Item> item = itemService.searchItem(description);
		item.stream().forEach(System.out::println);
	}

	private static void addMovement(final Connection connection, final Scanner scanner) throws SQLException {

		final Movimento movimento = new Movimento(connection);

		System.out.println("write in article_id and quantity:");
		final int idArticolo = scanner.nextInt();
		final int quantita = scanner.nextInt();

		try {
			movimento.add(idArticolo, quantita);
		} catch (final QuantityException e) {
			System.out.println("could not retrieve more elements than the actual amount");
		}

		System.out.println("type in any key to continue");
		scanner.next();
	}

	private static void giacenza(final Connection connection, final Scanner scanner) throws SQLException {
		final Movimento movimento = new Movimento(connection);
		System.out.println("write in id of the item");
		final int itemId = scanner.nextInt();
		System.out.println("qta di item con ID " + itemId + ": " + movimento.giacenza(itemId));

		System.out.println("type in any key to continue");
		scanner.next();
	}

	// "2024-02-09"
	private static void giacenzaAtSomeTime(final Connection connection, final Scanner scanner) throws SQLException {

		final Movimento movement = new Movimento(connection);

		System.out.println("write in id of the item");
		final int itemId = scanner.nextInt();

		String dateStr;
		String datePattern;

		System.out.println("Write in the date (year-month-day):");
		dateStr = scanner.next();
		datePattern = "\\d{4}-\\d{2}-\\d{2}";

		// nel caso venga fornito un input errato viene lanciata una RuntimeException
		if (!Pattern.matches(datePattern, dateStr)) {
			throw new RuntimeException();
		}

		java.sql.Date sqlDate = null;
		java.util.Date utilDate = null;

		try {
			final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			utilDate = sdf.parse(dateStr);

			// Converto a java.sql.Date
			sqlDate = new java.sql.Date(utilDate.getTime());

		} catch (final ParseException e) {
			System.out.println("Error parsing date: " + e.getMessage());
		}

		// creo la data del giorno dopo a sqlDate
		final Calendar calendar = Calendar.getInstance();
		calendar.setTime(utilDate);
		calendar.add(Calendar.DAY_OF_MONTH, 1);
		final java.sql.Date dateDayAfter = new Date(calendar.getTimeInMillis());
		final java.sql.Date sqlDateDayAfter = new java.sql.Date(dateDayAfter.getTime());

		final int x = movement.giacenzaAtDate(itemId, sqlDateDayAfter);
		System.out.println("qta di item: " + x);

		System.out.println("type in any key to continue");
		scanner.next();
	}

	private static void unmovedItems(final Connection connection, final Scanner scanner) throws SQLException {
		final Movimento movement = new Movimento(connection);
		final List<Item> unmoved = movement.unmovedItems();
		unmoved.stream().forEach(System.out::println);

		System.out.println("type in any key to continue");
		scanner.next();
	}

	private static void fewItems(final Connection connection, final Scanner scanner) {
		final List<Item> fewItems;
		try {
			final Movimento movement = new Movimento(connection);
			fewItems = movement.fewItems();
			fewItems.stream().forEach(System.out::println);

			System.out.println("type in any key to continue");
			scanner.next();

		} catch (final SQLException e) {
			System.out.println("something went wrong");
		}
	}

	private static void articlesMovedLatestMonth(final Connection connection, final Scanner scanner) throws SQLException {

		final Movimento movement = new Movimento(connection);
		final List<Item> latestMovedItems = movement.articlesMovedLatestMonth();
		latestMovedItems.stream().forEach(System.out::println);

		System.out.println("type in any key to continue");
		scanner.next();
	}

	private static void latestMonthMovements(final Connection connection, final Scanner scanner) {
		final List<MovedItem> latestMovedItemsDetailed;
		try {
			final Movimento movement = new Movimento(connection);
			latestMovedItemsDetailed = movement.latestMonthMovements();
			latestMovedItemsDetailed.stream().forEach(System.out::println);

		} catch (final SQLException e) {
			System.out.println("something went wrong");
		}

		System.out.println("type in any key to continue");
		scanner.next();

	}
}
