package com.azserve.academy.java.jdbc.model;

public record MovedItem(int id, String code, String description, int amount, java.sql.Date date) {

	public int getId() {
		return this.id;
	}

	public String getCode() {
		return this.code;
	}

	public String getDescription() {
		return this.description;
	}

	public int getAmount() {
		return this.amount;
	}

	public java.sql.Date getDate() {
		return this.date;
	}

	public Item getItem() {
		return new Item(this.getId(), this.getCode(), this.getDescription());
	}

}
